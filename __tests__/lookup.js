const dns = require('dns');
const util = require('util');
const dgram = require('dgram');
const { advanceTo } = require('jest-date-mock');

const createLookup = require('..');
const {
  parseMessage,
  createMessage
} = require('../dns');

let server;

beforeEach((done) => {
  advanceTo(0);

  server = dgram.createSocket('udp4');
  server.on('error', (err) => {
    server.close();
    done.fail(err);
  });
  server.bind(() => {
    const address = server.address();
    const serverAddress = `127.0.0.1:${address.port}`
    dns.setServers([serverAddress]);
    done();
  });
});

afterEach((done) => {
  server.close(done);
});

test('makes only one DNS query for concurrent requests to a hostname', async () => {
  let callCount = 0;
  server.on('message', (msg, { port, address }) => {
    callCount += 1;
    const request = parseMessage(msg);
    const response = createMessage({
      id: request.id,
      questions: request.questions,
      answers: request.questions.map(() => ({
        address: '12.23.34.45',
        ttl: 123,
      }))
    });
    server.send(response, port, address);
  });
  const lookup = util.promisify(createLookup());

  const addresses = await Promise.all([
    lookup('shuheikagawa.com'),
    lookup('shuheikagawa.com'),
    lookup('shuheikagawa.com')
  ]);

  expect(addresses).toEqual([
    '12.23.34.45',
    '12.23.34.45',
    '12.23.34.45'
  ]);
  // Make sure concurrent DNS calls make only one DNS query.
  expect(callCount).toBe(1);
});

test('makes a DNS query for each hostname', async () => {
  let callCount = 0;
  server.on('message', (msg, { port, address }) => {
    callCount += 1;
    const request = parseMessage(msg);
    const answers = request.questions
      .map(q => {
        const address = q.qname === 'shuheikagawa.com' ? '12.23.34.45' : '8.9.10.11';
        return { address, ttl: 123 };
      });
    const response = createMessage({
      id: request.id,
      questions: request.questions,
      answers
    });
    server.send(response, port, address);
  });
  const lookup = util.promisify(createLookup());

  const addresses = await Promise.all([
    lookup('shuheikagawa.com'),
    lookup('foobar.com'),
    lookup('shuheikagawa.com')
  ]);

  expect(addresses).toEqual([
    '12.23.34.45',
    '8.9.10.11',
    '12.23.34.45',
  ]);
  expect(callCount).toBe(2);
});

test('invalidates cache after TTL', async () => {
  let callCount = 0;
  server.on('message', (msg, { port, address }) => {
    callCount += 1;
    const request = parseMessage(msg);
    const response = createMessage({
      id: request.id,
      questions: request.questions,
      answers: request.questions.map(q => ({
        address: '12.23.34.45',
        ttl: 123
      }))
    });
    server.send(response, port, address);
  });
  const lookup = util.promisify(createLookup());

  expect(await lookup('shuheikagawa.com')).toEqual('12.23.34.45');

  advanceTo(100 * 1000);
  expect(await lookup('shuheikagawa.com')).toEqual('12.23.34.45');
  expect(callCount).toBe(1);

  advanceTo(123 * 1000 + 1);
  expect(await lookup('shuheikagawa.com')).toEqual('12.23.34.45');
  expect(callCount).toBe(2);
});
