const util = require('util');
const dns = require('dns');

function createLookup({
  bufferAfterTTL = 0,
} = {}) {
  const resolve4 = util.promisify(dns.resolve4);
  const cache = new Map();
  function cachedLookup(hostname, options, callback) {
    const cb = callback || options;
    let cacheEntry = cache.get(hostname);
    if (!cacheEntry) {
      cacheEntry = new LookupCacheEntry(hostname, resolve4, bufferAfterTTL);
      cache.set(hostname, cacheEntry);
    }
    cacheEntry.get().then(
      (addresses) => {
        // Choose an address with round-robin.
        const index = Math.floor(Math.random() * addresses.length);
        const address = addresses[index];
        cb(null, address.address);
      },
      (err) => {
        cb(err);
      },
    );
  }
  return cachedLookup;
}

/*
type Record = {
  address: string,
  ttl: number,
};

type Resolve = (hostname: string, options: { ttl: boolean }) => Promise<Record[]>;
*/

class LookupCacheEntry {
  /*
  bufferAfterTTL: number;
  resolve: Resolve;

  current: Record[] | null;
  next: Promise<Record[]> | null;
  */

  constructor(hostname, resolve, bufferAfterTTL) {
    this.hostname = hostname;
    this.resolve = resolve;
    this.bufferAfterTTL = bufferAfterTTL;

    this.current = null;
    this.next = null;
  }

  get() {
    const now = Date.now();
    if (this.current && this.current.every(record => record.expiresAt >= now)) {
      // Fresh cache!
      return Promise.resolve(this.current);
    }
    // No fresh cache.
    if (!this.next) {
      // No inflight request. Let's make a request!
      // TODO: Add timeout?
      this.next = this.resolve(this.hostname, { ttl: true })
        .then((result) => {
          const now = Date.now();
          // TTL is in seconds.
          this.current = result.map(record => Object.assign({
            expiresAt: now + record.ttl * 1000
          }, record));
          this.next = null;
        }, (err) => {
          this.next = null;
          throw err;
        });
    }
    // There is a inflight request. Let's wait for it.
    return this.next
      .then(() => {
        // Use the freshly set result.
        return this.current;
      })
      .catch((err) => {
        const now = Date.now();
        if (this.current && this.current.every(record => record.expiresAt + this.bufferAfterTTL >= now)) {
          // Use a bit stale value as a fallback if it's not too old.
          return this.current;
        }
        throw err;
      });
  }
}

module.exports = createLookup;
