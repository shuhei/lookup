// https://routley.io/tech/2017/12/28/hand-writing-dns-messages.html
// https://ops.tips/blog/raw-dns-resolver-in-go/
// http://www-inf.int-evry.fr/~hennequi/CoursDNS/NOTES-COURS_eng/msg.html
function parseMessage(buffer) {
  const id = buffer.readUInt16BE(0);

  // The third byte
  const third = buffer[2];
  // query (0) or response (1)
  const qr = third >> 7;
  const opcode = (third >> 3) & 0b1111;
  // ??
  const aa = (third >> 2) & 1;
  // truncated or not
  const tc = (third >> 1) & 1;
  // recursion desired
  const rd = third & 1;

  const fourth = buffer[3];
  // recursion available
  const ra = fourth >> 7;
  // ???
  const z = (fourth >> 4) & 0b111;
  // Error codes
  const rcode = fourth & 0b1111;

  // question count
  const qdcount = buffer.readUInt16BE(4);
  // answer count
  const ancount = buffer.readUInt16BE(6);
  // authority records count
  const nscount = buffer.readUInt16BE(8);
  // additional records count
  const arcount = buffer.readUInt16BE(10);

  let index = 12;
  const questions = [];
  for (let i = 0; i < qdcount; i++) {
    const { question, next } = parseQuestion(buffer, index);
    questions.push(question);
    index = next;
  }
  return {
    id,
    qr,
    opcode,
    aa,
    tc,
    rd,
    ra,
    z,
    rcode,
    qdcount,
    ancount,
    nscount,
    arcount,
    questions
  };
}

function parseQuestion(buffer, start) {
  let index = start;
  const qname = [];
  while (index < buffer.length) {
    const size = buffer.readUInt8(index);
    index += 1;
    if (size === 0) {
      break;
    }
    // TODO: URL decoding
    const label = buffer.slice(index, index + size).toString('utf8');
    index += size;
    qname.push(label);
  }
  const qtype = buffer.readUInt16BE(index);
  index += 2;
  const qclass = buffer.readUInt16BE(index);
  index += 2;
  const question = {
    qname: qname.join('.'),
    qtype,
    qclass,
  };
  return {
    question,
    next: index,
  };
}

function sum(nums) {
  return nums.reduce((acc, num) => acc + num, 0);
}

function questionSize(question) {
  // TODO: Label's URL encoding
  const qnameSize = sum(question.qname.split('.').map(label => 1 + label.length)) + 1;
  return qnameSize + 4;
}

const HEADER_SIZE = 12;
const ANSWER_SIZE = 16;
function createMessage({
  id,
  questions,
  answers,
}) {
  const questionSizes = questions.map(questionSize);
  const size = HEADER_SIZE + sum(questionSizes) + ANSWER_SIZE * answers.length;
  const buffer = Buffer.alloc(size);

  buffer.writeUInt16BE(id, 0);
  // QR = 1 (response), AA = 0 (not authority), RD = 1 (recursion was desired)
  buffer.writeUInt8(0x81, 2);
  // RA = 1 (recursion is available), RCODE = 0 (no errors)
  buffer.writeUInt8(0x80, 3);
  buffer.writeUInt16BE(questions.length, 4);
  buffer.writeUInt16BE(answers.length, 6);
  // no authority records
  buffer.writeUInt16BE(0, 8);
  // no additional records
  buffer.writeUInt16BE(0, 10);

  let index = HEADER_SIZE;

  for (let i = 0; i < questions.length; i++) {
    const question = questions[i];
    const qname = question.qname.split('.');
    for (let j = 0; j < qname.length; j++) {
      const label = qname[j];
      buffer.writeUInt8(label.length, index);
      index += 1;
      // TODO: URL encoding
      buffer.write(label, index);
      index += label.length;
    }
    // Terminate qname with NULL
    buffer.writeUInt8(0, index);
    index += 1;
    buffer.writeUInt16BE(question.qtype, index);
    index += 2;
    buffer.writeUInt16BE(question.qclass, index);
    index += 2;
  }

  let offset = HEADER_SIZE;
  for (let i = 0; i < answers.length; i++) {
    const answer = answers[i];
    const address = answer.address.split('.').map(s => parseInt(s, 10));
    const name = (0b11 << 14) | offset;
    offset += questionSizes[i];
    buffer.writeUInt16BE(name, index);
    index += 2;
    // type
    buffer.writeUInt16BE(1, index);
    index += 2;
    // class
    buffer.writeUInt16BE(1, index);
    index += 2;
    // TTL
    buffer.writeUInt32BE(answer.ttl, index);
    index += 4;
    // rdlength
    buffer.writeUInt16BE(4, index);
    index += 2;
    // rddata
    buffer.writeUInt8(address[0], index);
    buffer.writeUInt8(address[1], index + 1);
    buffer.writeUInt8(address[2], index + 2);
    buffer.writeUInt8(address[3], index + 3);
    index += 4;
  }

  return buffer;
}

module.exports = {
  parseMessage,
  createMessage,
};
